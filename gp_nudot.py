
from __future__ import print_function
import numpy as np
from sklearn.gaussian_process import GaussianProcessRegressor as GPR
from sklearn.gaussian_process.kernels import RBF, WhiteKernel, Matern
import matplotlib.pyplot as plt
from scipy.linalg import lapack as lp
from scipy import spatial
import tempfile
import logging
import argparse
import os
import sys

class calcNudot():

    def __init__(self, ephemeris, residuals, cadence=None):

        self.ephemeris = ephemeris # Pulsar ephemeris
        self.residuals = residuals # Timing residuals
        self.cadence = cadence # Cadence

    def _check_file(self, this_file):
        """
        Checks our input files exist and 
        exits if not
        """
        outcome = True
        if not os.path.isfile(this_file):
            print("{} not found".format(this_file))
            outcome = False
        return outcome

    def _read_eph(self, eph):
        """
        Reads ephemeris and extracts 
        period and period derivative
        """
        with open(eph, 'r') as this_eph:
            for line in this_eph.readlines():
                fields = line.split()
                if fields[0] == "F0":
                    period = 1 / float(fields[1])
                if fields[0] == "F1":
                    fdot = float(fields[1])

            return period, fdot

    @staticmethod
    def get_opt_pars(model):
        variance = np.exp(model.kernel_.theta[0])
        lengthscale = np.exp(model.kernel_.theta[1])
        noisevar = np.exp(model.kernel_.theta[2])
        return variance, lengthscale, noisevar

    def make_cov(self, mjd):
        K = []
        for i in range(0, len(mjd)):
            this_cov = []
            for j in range(0, len(mjd)):
                this_cov.append(self.get_cov(i, 
                    j, 
                    mjd[i], 
                    mjd[j], 
                    self.opt_pars[0],
                    self.opt_pars[1],
                    self.opt_pars[2]))
            K.append(this_cov)

        K = np.asarray(K)
        return K

    @staticmethod
    def get_cov(i, j, resi, resj, var, lengthscale, noisevar):
        if i != j:
            cov = var * np.exp(-0.5 * (resi - resj)**2.0 * (1.0 / lengthscale**2.0))
        else:
            cov = var * np.exp(-0.5 * (resi - resj)**2.0 * (1.0 / lengthscale**2.0)) + noisevar

        return cov[0]

    def pdinv(self, A, *args):
        """
        :param A: A DxD pd numpy array

        :rval Ai: the inverse of A
        :rtype Ai: np.ndarray
        :rval L: the Cholesky decomposition of A
        :rtype L: np.ndarray
        :rval Li: the Cholesky decomposition of Ai
        :rtype Li: np.ndarray
        :rval logdet: the log of the determinant of A
        :rtype logdet: float64

        """
        L = self.jitchol(A, *args)
        logdet = 2.*np.sum(np.log(np.diag(L)))
        Li = lp.dtrtri(L)
        Ai, _ = lp.dpotri(L, lower=1)
        # Ai = np.tril(Ai) + np.tril(Ai,-1).T
        self.symmetrify(Ai)

        return Ai, L, Li, logdet

    def jitchol(self, A, maxtries=5):
        A = np.ascontiguousarray(A)
        L, info = lp.dpotrf(A, lower=1)
        if info == 0:
            return L
        else:
            diagA = np.diag(A)
            if np.any(diagA <= 0.):
                raise linalg.LinAlgError("not pd: non-positive diagonal elements")
            jitter = diagA.mean() * 1e-6
            num_tries = 1
            while num_tries <= maxtries and np.isfinite(jitter):
                try:
                    L = linalg.cholesky(A + np.eye(A.shape[0]) * jitter, lower=True)
                    return L
                except:
                    jitter *= 10
                finally:
                    num_tries += 1
            raise linalg.LinAlgError("not positive definite, even with jitter.")
        import traceback
        try: raise
        except:
            logging.warning('\n'.join(['Added jitter of {:.10e}'.format(jitter),
                '  in '+traceback.format_list(traceback.extract_stack(limit=3)[-2:-1])[0][2:]]))
        return L

    def symmetrify(self, A, upper=False):
        """
        Take the square matrix A and make it symmetrical by copting elements from
        the lower half to the upper

        works IN PLACE.

        note: tries to use cython, falls back to a slower numpy version
        """
        self.symmetrify_numpy(A, upper)


    def symmetrify_numpy(self, A, upper=False):
        triu = np.triu_indices_from(A,k=1)
        if upper:
            A.T[triu] = A[triu]
        else:
            A[triu] = A.T[triu]

    def DKD(self, X1, X2, theta):

        X1, X2 = np.matrix(X1), np.matrix(X2) # ensure both sets of inputs are matrices

        D2 = spatial.distance.cdist(X1, X2, 'sqeuclidean') # calculate squared Euclidean distance
        D1 = np.zeros((X1.shape[0],X2.shape[0]))
        K = theta[0] * np.exp(- D2 / (2*(theta[1]**2))) * ( theta[1]**2 - D2) / theta[1]**4

        return np.matrix(K)

    def make_res_plot(self, mjds, resmod, res, err):
        assert len(resmod) == len(res)
        resres = []
        for i in range(0, len(mjds)):
            resres.append(resmod[i] - res[i])
        plt.errorbar(mjds, resres, yerr=err, marker='.', color='k', ecolor='k', linestyle='None')
        plt.xlabel("Days since period epoch", fontsize=15)
        plt.ylabel("Data - model", fontsize=15)
        plt.tight_layout()
        plt.show()

    def run(self):

        # Check files exists and exit if one or both is missing
        print("Checking files exists")
        if self._check_file(self.residuals) and \
           self._check_file(self.ephemeris):
            pass
        else:
            sys.exit(9)

        # Extract period and frequency derivative from ephemeris
        print("Reading parameters from ephemeris")
        self.period, self.f1 = self._read_eph(self.ephemeris)

        # Load in residuals
        print("Loading residuals")
        mjd, res, err = np.loadtxt(self.residuals, unpack=True, usecols=[0,1,2])

        # Reshape mjd array according to scikit learn's requirements
        mjd = np.reshape(mjd, (len(mjd), 1))

        # Define kernel function
        # This is the radial basis function combined with a white kernel
        K_WHITE = WhiteKernel(noise_level=1, noise_level_bounds=(1e-8, 1e+1))
        K_RBF = 1.0 * RBF(length_scale=500) + K_WHITE

        # Set up GP model
        print("Setting up GP model")
        gp_model = GPR(
            kernel=K_RBF,
            alpha=0.0,
            n_restarts_optimizer = 10)

        # Get initial hyperparameters
        init_hyp = gp_model.kernel
        print("\nInitial hyperparameters:\n{}\n".format(init_hyp))

        # Train GP model on input data
        print("Training GP on residuals...")
        gp_model.fit(mjd, res)

        # Get optmised hyperparameters
        opt_hyp = gp_model.kernel_
        print("\nOptimised hyperparameters:\n{}\n".format(opt_hyp))
        self.opt_pars = self.get_opt_pars(gp_model)

        # Get max log likelihood for optmised hyperparameters
        maxlik = gp_model.log_marginal_likelihood(gp_model.kernel_.theta)
        print("Max log-likelihood is {}".format(maxlik))

        # Set the mjds at which we want to infer the residuals
        # For now this is just the mjds at which we observed
        if self.cadence:
            mjdinfer = np.arange(mjd[0][0], mjd[-1][0], float(self.cadence))
            mjdinfer = np.reshape(mjdinfer, (len(mjdinfer), 1))
            #print(np.shape(mjdinfer))
        else:
            mjdinfer = mjd 
            #print(np.shape(mjdinfer))

        # Infer residual at our mjds (test epochs) based on trained GP
        res_mean, res_cov = gp_model.predict(mjdinfer, return_cov=True)

        # To do: Make residuals plots 

        # Get variance at each of our test epochs
        # This is the square root of the diagonals of our covariance matrix
        sig = 1.0 * np.sqrt(np.diag(res_cov))
       
        # Reformulate arrays for next steps of processing
        res_mean = np.asarray(res_mean)
        sig = np.asarray(sig)

        # Calculate residuals of residuals
        self.make_res_plot(mjdinfer, res_mean, res, sig)

        print("\nConstructing covariance matrix")
        self.K = self.make_cov(mjd)

        print("\nInverting covariance matrix")
        K1invOut = self.pdinv(np.matrix(self.K))
        K1inv = K1invOut[1]

        print("\nSetting prediction epochs")
        XTRAINING = mjd
        XPREDICT = mjd
        YTRAINING = np.matrix(np.array(res.flatten())).T

        print("\nCalculating nudot")
        covFunc = self.DKD
        par = np.zeros(2)
        par[0] = self.opt_pars[0]
        par[1] = self.opt_pars[1]
        K_prime = covFunc(XPREDICT, XTRAINING, par)
        K_prime_p = 3*par[0]/par[1]**4
        KiKx, _ = lp.dpotrs(K1inv, np.asfortranarray(K_prime.T), lower = 1)

        nudot = np.array(self.f1  + np.dot(KiKx.T, YTRAINING)/self.period/(86400)**2)
        nudot_err = np.array(np.sqrt(K_prime_p - np.sum(np.multiply(KiKx, K_prime.T),0).T)/(86400)**2)

        plt.errorbar(mjdinfer, nudot/1e-15, yerr=nudot_err/1e-15, color='k', ecolor='k', marker='.')
        plt.xlabel("Days since period epoch", fontsize=15)
        plt.ylabel("Frequency derivative [1e-15 Hz/s]", fontsize=15)
        plt.tight_layout()
        plt.savefig("nudot.png", format='png', dpi=400)
        plt.show()

def main():

    parser = argparse.ArgumentParser(description='Calculates the evolution of the spin-down rate from pulsar residuals using Gaussian Process Regression')
    parser.add_argument('-e','--ephemeris', help='Pular Ephemeris', required=True)
    parser.add_argument('-r','--residuals', help='File of residuals [mjd, res, err]', required=True)
    parser.add_argument('-c','--cadence', help='Cadence of test epochs (broken - no idea why yet)', required=False)
    args = parser.parse_args()

    measure = calcNudot(args.ephemeris, args.residuals, args.cadence)
    measure.run()

if __name__ == '__main__':
    main()
