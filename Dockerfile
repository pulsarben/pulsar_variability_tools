FROM fedora:29
MAINTAINER ben@perfectsquares.net

WORKDIR /home
COPY requirements.txt $WORKDIR

# Install dependencies
RUN dnf -y update && \
    dnf -y install  \
    python3-devel \
    python3-pip \ 
    tcsh \
    git \ 
    rsync \ 
    autoconf automake libtool make gcc-c++ gcc-gfortran fftw-devel fftw-doc cfitsio cfitsio-devel && \ 
    pip3 install --no-cache-dir -r requirements.txt

# Install psrchive
WORKDIR /home/soft/
ENV TEMPO2=/usr/local/tempo2

# Download and bootstrap psrchive
#RUN  git clone git://git.code.sf.net/p/psrchive/code psrchive && \ 
RUN  git clone https://pulsarben@bitbucket.org/pulsarben/psrchive.git && \ 
     cd psrchive && \ 
     ./bootstrap && \
     ./configure || true 

# Install epsic
RUN  cd psrchive/packages && \ 
     make && \ 
     cd .. && \
     ./packages/epsic.csh
     
# Install tempo2
WORKDIR /home/soft/psrchive
RUN ./packages/tempo2.csh

# Build psrchive
WORKDIR /home/soft/psrchive
RUN ./configure && \
    make && \
    make check && \
    make install

WORKDIR /home/soft/tools
COPY gp_nudot.py $WORKDIR

